﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
        public string Country { get; set; }
        public string Duration { get; set; }

        public virtual ICollection<Cast> Casts { get; set; }
        public virtual ICollection<Direction> Directions { get; set; }
        public virtual ICollection<MovieGenre> MovieGenres { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Rating
    {
        public int MovieId { get; set; }
        public int ReviewerId { get; set; }
        public double ReviewerStars { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual Reviewer Reviewer { get; set; }
    }
}

﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data
{
    public class PrometheusContext : DbContext
    {
        public PrometheusContext(DbContextOptions<PrometheusContext> options) : base(options)
        {

        }

        public DbSet<Actor> Actors { get; set; }
        public DbSet<Cast> Casts { get; set; }
        public DbSet<Direction> Directions { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieGenre> MovieGenres { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Reviewer> Reviewers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Actor>(ConfigureActor);
            builder.Entity<Cast>(ConfigureCast);
            builder.Entity<Direction>(ConfigureDirection);
            builder.Entity<Director>(ConfigureDirector);
            builder.Entity<Genre>(ConfigureGenre);
            builder.Entity<Movie>(ConfigureMovie);
            builder.Entity<MovieGenre>(ConfigureMovieGenre);
            builder.Entity<Rating>(ConfigureRating);
            builder.Entity<Reviewer>(ConfigureReviewer);
        }

        private void ConfigureActor(EntityTypeBuilder<Actor> model)
        {
            model.HasKey(a => a.Id);
            model.Property(a => a.Id).UseSqlServerIdentityColumn();
            model.HasMany<Cast>().WithOne(c => c.Actor);
        }

        private void ConfigureCast(EntityTypeBuilder<Cast> model)
        {
            model.HasKey(c => new { c.ActorId, c.MovieId });
        }

        private void ConfigureDirection(EntityTypeBuilder<Direction> model)
        {
            model.HasKey(dn => new { dn.DirectorId, dn.MovieId });
        }

        private void ConfigureDirector(EntityTypeBuilder<Director> model)
        {
            model.HasKey(dr => dr.Id);
            model.HasMany(dr => dr.Directions).WithOne(dn => dn.Director);
        }

        private void ConfigureGenre(EntityTypeBuilder<Genre> model)
        {
            model.HasKey(g => g.Id);
            model.Property(g => g.Id).UseSqlServerIdentityColumn();
            model.HasMany<MovieGenre>(g => g.MovieGenres).WithOne(mg => mg.Genre);
        }

        private void ConfigureMovie(EntityTypeBuilder<Movie> model)
        {
            model.HasKey(m => m.Id);
            model.Property(m => m.Id).UseSqlServerIdentityColumn();
            model.HasMany<Cast>(m => m.Casts).WithOne(c => c.Movie);
            model.HasMany<Direction>(m => m.Directions).WithOne(dn => dn.Movie);
            model.HasMany<MovieGenre>(m => m.MovieGenres).WithOne(mg => mg.Movie);
            model.HasMany<Rating>(m => m.Ratings).WithOne(r => r.Movie);
        }

        private void ConfigureMovieGenre(EntityTypeBuilder<MovieGenre> model)
        {
            model.HasKey(mg => new { mg.GenreId, mg.MovieId });
        }

        private void ConfigureRating(EntityTypeBuilder<Rating> model)
        {
            model.HasKey(r => new {r.MovieId, r.ReviewerId});
        }

        private void ConfigureReviewer(EntityTypeBuilder<Reviewer> model)
        {
            model.HasKey(rv => rv.Id);
            model.Property(rv => rv.Id).UseSqlServerIdentityColumn();
            model.HasMany<Rating>(rv => rv.Ratings).WithOne(r => r.Reviewer);
        }
    }
}

﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Identity
{
    public class PrometheusIdentityDbContext : IdentityDbContext
    {
        public PrometheusIdentityDbContext(DbContextOptions<PrometheusIdentityDbContext> options)
            : base(options)
        {
        }
    }
}
